function Pokemon(name, lvl, hp) {
	this.name = name;
	this.level = lvl;
	this.health = hp * 2;
	this.attack = lvl;
	this.tackle =  function(target) {
		console.log(`${this.name} tackled ${target.name}.`);
        target.health -= this.attack;
		console.log(`${target.name}'s health is now reduced to ${target.health}.`);

        if(target.health < 10) {target.faint()}
	};
	this.faint = function() {
		console.log(`${this.name} fainted`);
	}
}

let eevee = new Pokemon("Eevee", 2, 15);
let blastoise = new Pokemon("Blastoise", 8, 20);

blastoise.tackle(eevee);
eevee.tackle(blastoise);
blastoise.tackle(eevee);
eevee.tackle(blastoise);
blastoise.tackle(eevee);